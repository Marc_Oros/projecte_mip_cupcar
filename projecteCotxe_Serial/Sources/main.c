#include "common.h"
#include "TFC/TFC.h"

stateMachine_t machine;
static uint8_t path[PATH_SIZE];
static short   pathIdx=0;
static short   currStep=0;

static stateTransMatrixRow_t stateTransMatrix[] =
{
		{ST_INIT, EV_ANY, ST_MAKEPATH},
		{ST_MAKEPATH, EV_BUT_INVALID, ST_MAKEPATH},
		{ST_MAKEPATH, EV_BUT_UP, ST_ADDUP},
		{ST_MAKEPATH, EV_BUT_DOWN, ST_ADDDOWN},
		{ST_MAKEPATH, EV_BUT_LEFT, ST_ADDLEFT},
		{ST_MAKEPATH, EV_BUT_RIGHT, ST_ADDRIGHT},
		{ST_ADDUP, EV_ANY, ST_MAKEPATH},
		{ST_ADDDOWN, EV_ANY, ST_MAKEPATH},
		{ST_ADDLEFT, EV_ANY, ST_MAKEPATH},
		{ST_ADDRIGHT, EV_ANY, ST_MAKEPATH},
		{ST_MAKEPATH, EV_BUT_OK, ST_RUN},
		{ST_MAKEPATH, EV_FULL_LIST, ST_RUN},
		{ST_RUN, EV_NOTEMPTY_LIST, ST_RUN},
		{ST_RUN, EV_EMPTY_LIST, ST_INIT}		
};

//Problemes amb events que no haurien de passar en certs estats
event_t StateMachine_GetEvent()
{
	uint8_t keyCode;
	if(machine.currState == ST_MAKEPATH)
	{
		if(pathIdx == PATH_SIZE)
			return EV_FULL_LIST;
		while(CDC1_GetCharsInRxBuf()==0);
		CDC1_GetChar(&keyCode);
		CDC1_ClearRxBuffer();
		switch(keyCode)
		{
			case '1':
				return EV_BUT_UP;
			case '2':
				return EV_BUT_DOWN;
			case '3':
				return EV_BUT_LEFT;
			case '4':
				return EV_BUT_RIGHT;
			case '5':
				return EV_BUT_OK;
			default:
				return EV_BUT_INVALID;
		}
				
	}
	else
	{
		if(machine.currState == ST_RUN)
		{
			if(currStep == pathIdx)
			{
				return EV_EMPTY_LIST;
			}
			else
			{
				return EV_NOTEMPTY_LIST;
			}
		}
		else
		{
			return EV_ANY;
		}
	}
}

static void CDC_Init(void) {
	static uint8_t cdc_buffer[USB1_DATA_BUFF_SIZE];
	while(CDC1_App_Task(cdc_buffer, sizeof(cdc_buffer))==ERR_BUSOFF) {
      //Enumeraci�n de dispositivo
	  WAIT1_Waitms(10);
	}  
}


void init()
{
	(void)CDC1_SendString((unsigned char*)"Inicializando");
	(void)CDC1_SendString((unsigned char*)"\r\n");
	currStep=0;
	pathIdx=0;
}

void makepath()
{
	char str[50];
	sprintf(str, "Construyendo camino, elementos actuales: %d", pathIdx);
	(void)CDC1_SendString((uint8_t*)str);
	(void)CDC1_SendString((unsigned char*)"\r\n");
}

void addUp()
{
	path[pathIdx]=1;
	pathIdx++;
	(void)CDC1_SendString((unsigned char*)"Anadiendo UP");
	(void)CDC1_SendString((unsigned char*)"\r\n");
}

void addDown()
{
	path[pathIdx]=2;
	pathIdx++;
	(void)CDC1_SendString((unsigned char*)"Anadiendo DOWN");
	(void)CDC1_SendString((unsigned char*)"\r\n");
}

void addLeft()
{
	path[pathIdx]=3;
	pathIdx++;
	(void)CDC1_SendString((unsigned char*)"Anadiendo LEFT");
	(void)CDC1_SendString((unsigned char*)"\r\n");
}

void addRight()
{
	path[pathIdx]=4;
	pathIdx++;
	(void)CDC1_SendString((unsigned char*)"Anadiendo RIGHT");
	(void)CDC1_SendString((unsigned char*)"\r\n");
}

void run()
{
	if(currStep == pathIdx)	//En caso de camino vac�o, volvemos a inicializar
		return;
	char str[50];
	sprintf(str, "Movimiento %d", path[currStep]);
	(void)CDC1_SendString((uint8_t*)str);
	(void)CDC1_SendString((unsigned char*)"\r\n");	
	currStep++;
}

static stateFunctionRow_t stateFunctionA[] =
{
		{"ST_INIT", &init},
		{"ST_MAKEPATH", &makepath},
		{"ST_ADDUP", &addUp},
		{"ST_ADDDOWN", &addDown},
		{"ST_ADDLEFT", &addLeft},
		{"ST_ADDRIGHT", &addRight},
		{"ST_RUN", &run},
};

void StateMachine_RunIteration(stateMachine_t *stateMachine)
{
	int i=0;
	event_t event = StateMachine_GetEvent();
	 for(i; i < sizeof(stateTransMatrix)/sizeof(stateTransMatrix[0]); i++) {
		if(stateTransMatrix[i].currState == stateMachine->currState) {
			if((stateTransMatrix[i].event == event) || (stateTransMatrix[i].event == EV_ANY)) {
 
				// Transition to the next state
				stateMachine->currState =  stateTransMatrix[i].nextState;
 
				// Call the function associated with transition
				(stateFunctionA[stateMachine->currState].func)();
				break;
			}
		}
	}
}

int main(void)
{
	//Inicializaciones
	/*** Processor Expert internal initialization. DON'T REMOVE THIS CODE!!! ***/
	PE_low_level_init();  
	CDC_Init();
	TFC_Init();
	TFC_InitMotorPWM();
	machine.currState=ST_INIT;
	for(;;)
	{
		StateMachine_RunIteration(&machine);
	}

}

